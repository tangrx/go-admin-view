import layoutHeaderAside from '@/layout/header-aside'

const meta = { requiresAuth: true, cache: true }

export default {
  path: '/sys',
  name: 'sys',
  meta,
  redirect: { name: 'sys-user-index' },
  component: layoutHeaderAside,
  children: (pre => [
    { path: 'user/index', name: `${pre}user-index`, component: () => import('@/pages/sys/user/index'), meta: { ...meta, title: '用户管理' } },
    { path: 'user/edit/:id', name: `${pre}user-edit`, component: () => import('@/pages/sys/user/edit'), meta: { ...meta, title: '用户编辑' } },
    { path: 'user/add', name: `${pre}user-add`, component: () => import('@/pages/sys/user/add'), meta: { ...meta, title: '用户添加' } },
    { path: 'log/index', name: `${pre}log-index`, component: () => import('@/pages/sys/log/index'), meta: { ...meta, title: '系统日志' } },
    { path: 'config/index', name: `${pre}config-index`, component: () => import('@/pages/sys/config/index'), meta: { ...meta, title: '系统配置' } },
    { path: 'code/index', name: `${pre}code-index`, component: () => import('@/pages/sys/code/index'), meta: { ...meta, title: '数据字典' } },
    { path: 'auth/menu/index', name: `${pre}auth-menu-index`, component: () => import('@/pages/sys/menu/index'), meta: { ...meta, title: '菜单管理' } },
    { path: 'role/index', name: `${pre}role-index`, component: () => import('@/pages/sys/role/index'), meta: { ...meta, title: '角色管理' } }

  ])('sys-')
}
