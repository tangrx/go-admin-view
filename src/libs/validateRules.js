import { isMobile } from '@/libs/validate'
export function validateMobile (rule, value, callback) {
  let message = rule.message || '电话号码格式不正确!'
  if (rule.required && !value) {
    callback(new Error(rule.field + '必须输入！'))
  } else if (value) {
    if (isMobile(value)) {
      callback()
    } else {
      callback(new Error(message))
    }
  } else {
    callback()
  }
}
