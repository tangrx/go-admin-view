import request from '@/plugin/axios'

export function GetLogList (data) {
  return request({
    url: '/api/sys/log',
    method: 'post',
    data
  })
}

export function DeleteLog (data) {
  return request({
    url: '/api/sys/log/delete',
    method: 'post',
    data
  })
}
