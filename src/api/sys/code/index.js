import request from '@/plugin/axios'

export function GetCodeList (data) {
  return request({
    url: '/api/sys/code',
    method: 'post',
    data
  })
}

export function GetCodeTreeList (data) {
  return request({
    url: '/api/sys/codetree',
    method: 'post',
    data
  })
}

export function DeleteCode (data) {
  return request({
    url: '/api/sys/code/delete',
    method: 'post',
    data
  })
}

export function SaveCode (data) {
  return request({
    url: '/api/sys/code/save',
    method: 'post',
    data
  })
}
