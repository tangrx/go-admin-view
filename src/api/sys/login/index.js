import request from '@/plugin/axios'

export function AccountLogin (data) {
  return request.post('/auth/login', data)
}
// 获取用户菜单
export function GetMenus () {
  return request({
    url: '/auth/menu',
    method: 'post'
  })
}
