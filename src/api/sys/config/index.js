import request from '@/plugin/axios'

export function GetConfigList (data) {
  return request({
    url: '/api/sys/config',
    method: 'post',
    data
  })
}

export function Save (data) {
  return request({
    url: '/api/sys/config/save',
    method: 'post',
    data
  })
}
