import request from '@/plugin/axios'

export function GetRoleList (data) {
  return request({
    url: '/api/sys/role',
    method: 'post',
    data
  })
}

export function Save (data) {
  return request({
    url: '/api/sys/role/save',
    method: 'post',
    data
  })
}
