import request from '@/plugin/axios'

export function GetMenuList (data) {
  return request({
    url: '/api/sys/menu',
    method: 'post',
    data
  })
}

export function GetMenuTreeList (data) {
  return request({
    url: '/api/sys/menutree',
    method: 'post',
    data
  })
}

export function DeleteMenu (data) {
  return request({
    url: '/api/sys/menu/delete',
    method: 'post',
    data
  })
}

export function SaveMenu (data) {
  return request({
    url: '/api/sys/menu/save',
    method: 'post',
    data
  })
}
