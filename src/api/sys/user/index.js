import request from '@/plugin/axios'

export function GetUserList (data) {
  return request({
    url: '/api/sys/user',
    method: 'post',
    data
  })
}

export function GetUserData (data) {
  return request({
    url: '/api/sys/user/info',
    method: 'post',
    data
  })
}

export function SaveUserData (data) {
  return request({
    url: '/api/sys/user/save',
    method: 'post',
    data
  })
}

export function DeleteUser (data) {
  return request(
    {
      url: '/api/sys/user/delete',
      method: 'post',
      data
    }
  )
}
