export default {
  install (Vue, options) {
    const moment = options && options.moment ? options.moment : require('moment')

    Object.defineProperties(Vue.prototype, {
      $moment: {
        get () {
          return moment
        }
      }
    })

    Vue.moment = moment
    Vue.filter('moment', (...args) => {
      args = Array.prototype.slice.call(args)
      let input = args.shift()
      let fmt = args.shift()
      return moment(input).zone(0).format(fmt)
    })
  }
}
