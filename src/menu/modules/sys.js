export default {
  path: '/sys',
  title: '系统管理',
  icon: 'cogs',
  children: (pre => [
    { path: `${pre}index`, title: '用户列表', icon: 'group' }
  ])('/sys/user/')
}
