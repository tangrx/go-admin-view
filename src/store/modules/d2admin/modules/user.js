// 设置文件
import setting from '@/setting.js'

export default {
  namespaced: true,
  state: {
    // 用户信息
    info: setting.user.info,
    menu: null
  },
  actions: {
    /**
     * @description 设置用户数据
     * @param {Object} state vuex state
     * @param {*} info info
     */
    set ({ state, dispatch }, info) {
      return new Promise(async resolve => {
        // store 赋值
        state.info = info
        // 持久化
        await dispatch('d2admin/db/set', {
          dbName: 'sys',
          path: 'user.info',
          value: info,
          user: true
        }, { root: true })
        // end
        resolve()
      })
    },
    /**
     * @description 从数据库取用户数据
     * @param {Object} state vuex state
     */
    load ({ state, dispatch }) {
      return new Promise(async resolve => {
        // store 赋值
        state.info = await dispatch('d2admin/db/get', {
          dbName: 'sys',
          path: 'user.info',
          defaultValue: setting.user.info,
          user: true
        }, { root: true })

        state.menu = await this.dispatch('d2admin/db/get', {
          dbName: 'sys',
          path: 'user.menu',
          defaultValue: null,
          user: true
        }, { root: true })
        // end
        resolve()
      })
    },
    /**
     * 设置菜单
     */
    setMenu ({ state, dispatch }, menu) {
      return new Promise(async resolve => {
        // store 赋值
        state.menu = menu
        // 持久化
        await dispatch('d2admin/db/set', {
          dbName: 'sys',
          path: 'user.menu',
          value: menu,
          user: true
        }, { root: true })
        // end
        resolve()
      })
    }
  }
}
