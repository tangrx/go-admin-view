// polyfill
import 'babel-polyfill'
// Vue
import Vue from 'vue'
import App from './App'
// store
import store from '@/store/index'
// 多国语
import i18n from './i18n'
// 核心插件
import d2Admin from '@/plugin/d2admin'

// [ 可选插件组件 ]D2-Crud
import D2Crud from '@d2-projects/d2-crud'
// [ 可选插件组件 ] 图表
import VCharts from 'v-charts'
// [ 可选插件组件 ] 右键菜单
import contentmenu from 'v-contextmenu'
import 'v-contextmenu/dist/index.css'
// [ 可选插件组件 ] JSON 树状视图
import vueJsonTreeView from 'vue-json-tree-view'
// [ 可选插件组件 ] 网格布局组件
import { GridLayout, GridItem } from 'vue-grid-layout'
// [ 可选插件组件 ] 区域划分组件
import SplitPane from 'vue-splitpane'
// [ 可选插件组件 ] UEditor
import VueUeditorWrap from 'vue-ueditor-wrap'

// 菜单和路由设置
import router from './router'
// import { menuHeader, menuAside } from '@/menu'
import { frameInRoutes } from '@/router/routes'
import VueFormat from '@/plugin/format/index'
// 核心插件
Vue.use(d2Admin)

// 可选插件组件
Vue.use(D2Crud)
Vue.use(VCharts)
Vue.use(contentmenu)
Vue.use(vueJsonTreeView)
Vue.use(VueFormat)
Vue.component('d2-grid-layout', GridLayout)
Vue.component('d2-grid-item', GridItem)
Vue.component('SplitPane', SplitPane)
Vue.component('VueUeditorWrap', VueUeditorWrap)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  created () {
    // 设置侧边栏菜单
    this.$store.commit('d2admin/menu/asideSet')
    // 设置顶栏菜单
    this.$store.commit('d2admin/menu/headerSet')
    // 多页面控制: 处理路由 得到每一级的路由设置
    this.$store.commit('d2admin/page/init', frameInRoutes)
    // // 处理路由 得到每一级的路由设置
    // this.$store.commit('d2admin/page/init')
    // // 设置顶栏菜单
    // this.$store.commit('d2admin/menu/headerSet')
    // 初始化菜单搜索功能
    this.$store.commit('d2admin/search/init')
  },
  mounted () {
    // 展示系统信息
    this.$store.commit('d2admin/releases/versionShow')
    // 用户登录后从数据库加载一系列的设置
    this.$store.dispatch('d2admin/account/load')
    // 获取并记录用户 UA
    this.$store.commit('d2admin/ua/get')
    // 初始化全屏监听
    this.$store.dispatch('d2admin/fullscreen/listen')
  },
  watch: {
    // 监听路由 控制侧边栏显示
    '$route.matched' (val) {
      // const _side = menuAside.filter(menu => menu.path === val[0].path)
      // console.log(_side)
      this.$store.commit('d2admin/menu/asideSetForRouter', val)// _side.length > 0 ? _side[0].children : [])
    }
  }
}).$mount('#app')
